from mininet.cli import CLI
from mininet.link import Link, TCLink, Intf
from mininet.topo import Topo
from subprocess import Popen, PIPE
from mininet.log import setLogLevel
from mininet.net import Mininet
class MyTopo(Topo):
        def __init__(self):
                Topo.__init__(self)

                setLogLevel('info')
                net = Mininet(link=TCLink)
                key = "net.mptcp.mptcp_enabled"
                value = 0
                p = Popen("sysctl -w %s=%s" % (key, value), shell=True, stdout=PIPE, 	stderr=PIPE)
                stdout, stderr = p.communicate()
                print ("stdout=",stdout,"stderr=", stderr)

                HA = net.addHost('HA')
                HB = net.addHost('HB')
		R1 = net.addHost('R1')
		R2 = net.addHost('R2')
		R3 = net.addHost('R3')
		R4 = net.addHost('R4')

		bwlink={'bw':1}
		bwlink2={'bw':0.5}

		net.addLink(R1,HA,cls=TCLink,**bwlink)
		net.addLink(R1,R3,cls=TCLink,**bwlink2)
		net.addLink(R1,R4,cls=TCLink,**bwlink)

		net.addLink(R2,HA,cls=TCLink,**bwlink)
		net.addLink(R2,R4,cls=TCLink,**bwlink2)
		net.addLink(R2,R3,cls=TCLink,**bwlink)

		net.addLink(R3,HB,cls=TCLink,**bwlink)
		net.addLink(R4,HB,cls=TCLink,**bwlink) 

		net.build()

		HA.cmd("ifconfig HA-eth0 192.168.0.1 netmask 255.255.255.0")
		HA.cmd("ifconfig HA-eth1 192.168.7.1 netmask 255.255.255.0")

		HB.cmd("ifconfig HB-eth0 192.168.3.1 netmask 255.255.255.0")
		HB.cmd("ifconfig HB-eth1 192.168.4.1 netmask 255.255.255.0")

		R1.cmd("ifconfig R1-eth0 192.168.0.2 netmask 255.255.255.0")
		R1.cmd("ifconfig R1-eth1 192.168.1.1 netmask 255.255.255.0")
		R1.cmd("ifconfig R1-eth2 192.168.5.2 netmask 255.255.255.0")
		R1.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")

		R2.cmd("ifconfig R2-eth0 192.168.7.2 netmask 255.255.255.0")
		R2.cmd("ifconfig R2-eth1 192.168.6.2 netmask 255.255.255.0")
		R2.cmd("ifconfig R2-eth2 192.168.2.2 netmask 255.255.255.0")
		R2.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")

		R3.cmd("ifconfig R3-eth0 192.168.1.2 netmask 255.255.255.0")
		R3.cmd("ifconfig R3-eth1 192.168.2.1 netmask 255.255.255.0")
		R3.cmd("ifconfig R3-eth2 192.168.3.2 netmask 255.255.255.0")
		R3.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")

		R4.cmd("ifconfig R4-eth0 192.168.6.1 netmask 255.255.255.0")
		R4.cmd("ifconfig R4-eth1 192.168.5.1 netmask 255.255.255.0")
		R4.cmd("ifconfig R4-eth2 192.168.4.2 netmask 255.255.255.0")
		R4.cmd("echo 1 > /proc/sys/net/ipv4/ip_forward")

		HA.cmd("ip rule add from 192.168.0.1")
		HA.cmd("ip rule add from 192.168.7.1")
		HA.cmd("ip route add 192.168.0.0/24 dev h1-eth0 scope link table 1")
		HA.cmd("ip route add default via 192.168.0.2 dev h1-eth0 table 1")
		HA.cmd("ip route add 192.168.7.0/24 dev h1-eth1 scope link table 2")
		HA.cmd("ip route add default via 192.168.7.2 dev h1-eth1 table 2")
		HA.cmd("ip route add default scope global nexthope via 192.168.0.2 dev h1-eth0")

		HB.cmd("ip rule add from 192.168.3.1")
		HB.cmd("ip rule add from 192.168.4.1")
		HB.cmd("ip route add 192.168.3.0/24 dev h2-eth0 scope link table 3")
		HB.cmd("ip route add default via 192.168.3.2 dev h2-eth0 table 3")
		HB.cmd("ip route add 192.168.4.0/24 dev h1-eth1 scope link table 4")
		HB.cmd("ip route add default via 192.168.4.2 dev h1-eth1 table 4")
		HB.cmd("ip route add default scope global nexthope via 192.168.4.2 dev h2-eth1")

		R1.cmd("route add -net 192.168.2.0/24 gw 192.168.1.2")
		R1.cmd("route add -net 192.168.3.0/24 gw 192.168.1.2")
		R1.cmd("route add -net 192.168.6.0/24 gw 192.168.5.1")
		R1.cmd("route add -net 192.168.4.0/24 gw 192.168.5.1")
		R1.cmd("route add -net 192.168.7.0/24 gw 192.168.5.1")

		R2.cmd("route add -net 192.168.3.0/24 gw 192.168.2.1")
		R2.cmd("route add -net 192.1S68.4.0/24 gw 192.168.6.1")
		R2.cmd("route add -net 192.168.1.0/24 gw 192.168.2.1")
		R2.cmd("route add -net 192.168.0.0/24 gw 192.168.2.1")
		R2.cmd("route add -net 192.168.5.0/24 gw 192.168.6.1")

		R3.cmd("route add -net 192.168.0.0/24 gw 192.168.1.1")
		R3.cmd("route add -net 192.168.4.0/24 gw 192.168.2.2")
		R3.cmd("route add -net 192.168.7.0/24 gw 192.168.2.2")
		R3.cmd("route add -net 192.168.6.0/24 gw 192.168.2.2")
		R3.cmd("route add -net 192.168.5.0/24 gw 192.168.1.1")

		R4.cmd("route add -net 192.168.7.0/24 gw 192.168.6.2")
		R4.cmd("route add -net 192.168.0.0/24 gw 192.168.5.2")
		R4.cmd("route add -net 192.168.1.0/24 gw 192.168.5.2")
		R4.cmd("route add -net 192.168.3.0/24 gw 192.168.5.2")
		R4.cmd("route add -net 192.168.2.0/24 gw 192.168.6.2")

		#MPTCP STATIC ROUTING
		#Zeni Rahmatullah 1303190107
		net['hA'].cmd("ip rule add from 192.168.0.2 table 1")
		net['hA'].cmd("ip rule add from 192.168.0.2 table 2")
		net['hA'}.cmd("ip route add 192.168.0.0/24 dev hA-eth0 scope link table 1")
		net['hA'].cmd("ip route add default via 192.168.0.1 dev hA-eth0 table 1")
		net['hA'].cmd("ip route add 192.168.1.0/24 dev hA-eth1 scope link table 2")
		net['hA'}.cmd("ip route add default via 192.168.1.1 dev hA-eth1 table 2")
		net['hA'].cmd("ip route add default scope global nexthop via 192.168.0.1 dev hA-eth0")
		net['hB'].cmd("ip rule add from 192.168.4.2 table 1")
		net['hB'].cmd("ip rule add from 192.168.5.2 table 2")
		net['hB'].cmd("ip route add 192.168.4.0/24 dev hB-eth0 scope link table 1")
		net['hB'].cmd("ip route add default via 192.168.4.1 dev hB-eth0 table 1")
		net['hB'].cmd("ip route add 192.168.5.0/24 dev hB-eth1 scope link table 2")
		net['hB'].cmd("ip route add default via 192.168.5.1 dev hB-eth1 table 2")
		net['hB'].cmd("ip route add default scope global nexthop via 192.168.4.1 dev hB-eth0")

		CLI(net)
		net.stop()
S
topos={'mytopo':(lambda: MyTopo() ) }
